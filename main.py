import os

from lib import database
from lib import property


def menu():
    selection = 0
    while selection is not 5:
        os.system("clear")
        print("""
=================================================
PyProps v1.00
Written by: DJ Kemmet
Errata and requests: dkemmet@monogramres.com
Written in Python 3.5.1+
+++++++++++++++++++++++++++++++++++++++++++++++++
MENU
+++++++++++++++++++++++++++++++++++++++++++++++++
    1. Add A Property
    2. Remove a Property
    3. Edit A Property
    4. View Portfolio
    5. exit
================================================
        """)
        selection = input("Selection| ")
        if selection is "1":
            os.system("Clear")
            property.add()
        elif selection is "2":
            propertyName = input("Which Property would you like to remove?")
            property.remove(propertyName)
        elif selection is "3":
            print("Selection is 3")
        elif selection is "4":
            print("Selection is 4")
        elif selection is "5":
            print("Goodbye.")
            exit()


def startup():
    if database.initialize() is "true":
        menu()
    else:
        database.configure()
        menu()



startup()
